# Tarea Predictores de Saltos

Esta es la solución de la tarea de predictores de saltos.
Cuenta con un total de 4 predictores diferentes, el primero de ellos es un predictor bimodal, seguido de un predictor con historia privada, posteriormente un predictor con historia global y por último un predictor por torneo, que es una combinación de los dos últimos predictores antes mencionados.
Si se desea cambiar la cantidad de muestras a tomar, se debe abrir el codigo .py de cada predictor y dentro de los ciclos while que aparecen dentro de estos, se debe cambiar la línea que diga "len(lista)" por el número de muestras que se desean tomar.
Para correr el programa se debe ejecutar descargar el archivo .zip adjunto en el repositorio y se procede a descomprimirlo, seguidamente se debe ejecutar en una terminal el comando make, siguiendo lo siguiente:

Opción pred bimodal:
    make PRED0
Opción pred con historia privada:
    make PRED1
Opción pred con historia global:
    make PRED2
Opción pred por torneo:
    make PRED3

Los parámetros escritos en el makefile son los siguientes:

-> -s es el tamaño de la tabla BHT.

-> bp es el tipo de predicción de saltos a usar, hay tres opciones:
 bp = 0 <-> Bimodal
 bp = 1 <-> Con historia privada
 bp = 2 <-> Con historia global
 bp = 3 <-> De torneo

-> gh es el tamaño del registro de historia global

-> ph es el tamaño del registro de historia privada 

Cabe mencionar que si se desea cambiar alguno de estos, se deben de seguir las instrucciones que vienen dentro del makefile.

